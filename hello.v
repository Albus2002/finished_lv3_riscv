
  .text
  .globl main
main:
  addi sp, sp, -80
  sw ra, 0(sp)
  li t0, 8
  add t0, t0, sp
  sw t0, 4(sp)
  li t0, 1
  sw t0, 12(sp)
  lw t1, 4(sp)
  lw t0, 12(sp)
  sw t0, 0(t1)
  li t0, 11
  sw t0, 20(sp)
  li t0, 0
  sw t0, 24(sp)
  lw t0, 20(sp)
  lw t1, 24(sp)
  xor t0, t0, t1
  seqz t0, t0
  sw t0, 20(sp)
Label_0:
  li t0, 0
  sw t0, 32(sp)
  li t0, 0
  sw t0, 36(sp)
  lw t0, 32(sp)
  lw t1, 36(sp)
  xor t0, t0, t1
  snez t0, t0
  sw t0, 32(sp)
  lw t1, 4(sp)
  lw t0, 32(sp)
  sw t0, 0(t1)
 j Label_2
Label_1:
 j Label_2
Label_2:
  lw t0, 4(sp)
  lw t0, 0(t0)
  sw t0, 56(sp)
  lw a0, 56(sp)
  lw ra, 0(sp)
  addi sp, sp, 80
  ret
